#!/usr/bin/env python3

"""
 This Python script implements a Python-Snap7 to CSV Writer
 
 References :
 https://pypi.org/project/python-snap7/

 https://python-snap7.readthedocs.io/en/latest/index.html

"""

import signal
import time
import datetime as dt
import struct
import csv
import pandas as pd
import tarfile
import os

import snap7

running = True

def quit():
    global running
    running = False
    print("Main : User interrupt !")

def sig_handler(signum, frame):
    quit()

def main():
    """
    Python-Snap7 to CSV Writer
    """

    print("=======================================\n"
          "  Python-Snap7 to CSV Writer Main !\n"
          "=======================================\n")

    signal.signal(signal.SIGINT, sig_handler)

    PS7_Connect_Done = False
    PLC_IP_ADDR = os.environ.get("SNAP7_PLC_IP_ADDR", "192.168.253.240")
    PLC_RACK = int(os.environ.get("SNAP7_PLC_RACK", 0))
    PLC_SLOT = int(os.environ.get("SNAP7_PLC_SLOT", 3))
    PLC_PORT = 102

    snap7_client = snap7.client.Client()

    print("=========================================\n"
          "   IO Loop : Ctrl-c to terminate !\n"
          "=========================================\n")

    while running:

        if not PS7_Connect_Done:

            print("Main : Connection to PLC {ipaddr}/{rack}/{slot}"
                  .format(ipaddr = PLC_IP_ADDR,
                          rack = PLC_RACK, slot = PLC_SLOT))

            try:
                snap7_client.connect(PLC_IP_ADDR, PLC_RACK, PLC_SLOT, PLC_PORT)

            except RuntimeError as exc :
                print("Main : Error Connecting to PLC :\n", exc)
            else:
                PS7_Connect_Done = True

        else:

            today = dt.datetime.now(tz = dt.UTC).strftime("%Y%m%d")
            old_today = today
            csv_file_name = today + '.csv'
            tgz_file_name = today + '.tar.gz'

            with open(csv_file_name, 'a', newline = '',
                      encoding = 'utf-8') as f:
                writer = csv.writer(f)

                while running and PS7_Connect_Done:

                    today = dt.datetime.now(tz = dt.UTC).strftime("%Y%m%d")

                    if today != old_today:
                        break

                    try:
                        buffer = snap7_client.db_read(db_number = 1, start = 0,
                                                      size = 2)
                        db1_dbw0 = int.from_bytes(buffer[0:2],
                                                  byteorder = 'big',
                                                  signed = True)

                        buffer = snap7_client.eb_read(start = 0, size = 2)
                        ew0 = int.from_bytes(buffer[0:2],
                                             byteorder = 'big', signed = True)

                        buffer = snap7_client.ab_read(start = 0, size = 2)
                        aw0 = int.from_bytes(buffer[0:2],
                                             byteorder = 'big', signed = True)

                        buffer = snap7_client.mb_read(start = 0, size = 2)
                        mw0 = int.from_bytes(buffer[0:2],
                                             byteorder = 'big', signed = True)

                    except RuntimeError as exc :
                        print("Main : Error reading from PLC :\n", exc)
                        print("Main : Disconnect from PLC")
                        snap7_client.disconnect()
                        PS7_Connect_Done = False

                    else:
                        line = [pd.Timestamp.now(tz = dt.UTC),
                                db1_dbw0, ew0, aw0, mw0]
                        writer.writerow(line)
                        f.flush()

                        time.sleep(1.05)

            if today != old_today:
                try:
                    print("Main : Creating archive {}".format(tgz_file_name))
                    with tarfile.open(tgz_file_name, 'w:gz') as tar:
                        tar.add(csv_file_name)
                except Exception as exc :
                    print("Main : Error Creating archive :\n", exc)
                else:
                    os.remove(csv_file_name)

    if PS7_Connect_Done:
        print("Main : Disconnect from PLC")
        snap7_client.disconnect()
        PS7_Connect_Done = False

if __name__ == "__main__":
    main()


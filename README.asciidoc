= Python-Snap7 to CSV Writer
Stéphane LOS
v2024.03, 2024-03-18
:doctype: book
:lang: en
:toc: left
:toclevels: 4
:icons: font
:numbered:
:Author Initials: SLO
:source-highlighter: rouge
:imagesdir: images

== Description

=== Snap7-CSV.py

Snap7-CSV.py is a Python script that implements a Python-Snap7 to CSV Writer.

https://pypi.org/project/python-snap7/

https://python-snap7.readthedocs.io/en/latest/index.html

=== Matplotlib_read.py

Matplotlib_read.py reads, using Pandas, the CSV file produced by Snap7-CSV.py,
selects a time range and creates a plot, using Matplotlib, that can be used for
eg. reports.

If a ".tar.gz" file is given, CSV is extracted from it.

== Some notes

Create a virtual environment :

----
python3 -m venv ./venv
----

and activate it :

----
source venv/bin/activate
----

Install Python Snap7 wrapper

----
pip install python-snap7
----

Play in Interpreter

----
python
>>> import snap7
>>> client = snap7.client.Client()
>>> client.connect("192.168.253.240", 0, 3, 102)
>>> cpu_info = client.get_cpu_info()
>>> print(cpu_info)
>>> client.db_read(db_number = 1, start = 0, size = 2)
>>> client.eb_read(start = 0, size = 2)
>>> client.ab_read(start = 0, size = 2)
>>> client.mb_read(start = 0, size = 2)
>>> client.disconnect()
>>> client.destroy()
----

Install Pandas

----
pip install pandas
----

Snap7-CSV.py uses environment variables for PLC connection settings.
Those settings are available in plc_env_vars.
Export those with :

----
source plc_env_vars
----

Run script

----
python Snap7-CSV.py
----

Test with tail

----
tail -f 20240311.csv
----

Install Matplotlib

----
pip install matplotlib
----

Run script providing file name, either ".csv" or ".tar.gz" format, and optional
range selection timestamps start and end.

----
python Matplotlib_read.py 20240317.csv \
--start=2024-03-17\ 16:00:00 --end=2024-03-17\ 18:00:00
----

Enjoy the plot !

Freeze Requirements

----
pip freeze > requirements.txt
----



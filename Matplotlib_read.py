#!/usr/bin/env python3

"""
 This Python script reads, using Pandas, the CSV file produced by Snap7-CSV.py,
 selects a time range and creates a plot, using Matplotlib, that can be used for
 eg. reports.

 If a ".tar.gz" file is given, CSV is extracted from it.

"""


import argparse
import os.path
import tarfile

from datetime import datetime
from pytz import timezone

import matplotlib
import matplotlib.pyplot as plt
import pandas as pd

def init_argparser() -> argparse.ArgumentParser:

    parser = argparse.ArgumentParser(
             description = "Reads a CSV file and create a nice plot.")

    parser.add_argument('filename', help = "File to process")

    parser.add_argument('--start', help = "Selection start timestamp")

    parser.add_argument('--end', help = "Selection end timestamp")

    return parser

def main():
    """
    Matplotlib Read CSV Main
    """

    print("=======================================\n"
          "  Matplotlib Read CSV Main !  \n"
          "=======================================\n")

    parser = init_argparser()
    args = parser.parse_args()

    filename = args.filename
    print("Processing {filename}".format(filename = filename))

    if not os.path.isfile(args.filename):
        print("File does not exist !")
        exit()

    if filename[-4:] == ".csv":
        csv_file_name = filename
    elif filename[-7:] == ".tar.gz":
        tgz_file_name = filename
        try:
            print("Main : Opening archive {}".format(tgz_file_name))
            csv_file_name = filename[:-7] + ".csv"
            with tarfile.open(tgz_file_name, 'r:gz') as tar:
                tar.extract(csv_file_name)
        except Exception as exc :
            print("Main : Error Opening archive :\n", exc)
            exit()
    else:
        print("Unknown file !")
        exit()

    # Import data
    df = pd.read_csv(filepath_or_buffer = csv_file_name,
                     sep = ',',
                     header = None,
                     names = ['TimeStamp',
                              'db1_dbw0',
                              'ew0',
                              'aw0',
                              'mw0'],
                     index_col = 0,
                     parse_dates = [0],
                     on_bad_lines = 'skip')

    # Print some information
    print('\ndf information :\n')
    print(df.info())
    print('\ndf head :\n')
    print(df.head(10))
    print('\ndf tail :\n')
    print(df.tail(10))

    print('\ndf.index.min / max : {} / {}:'
          .format(df.index.min(), df.index.max()))

    date_format = "%Y-%m-%d %H:%M:%S"

    try:
        start = datetime.strptime(args.start, date_format)
        start = start.replace(tzinfo = timezone('UTC'))
    except Exception as exc :
        print("Main : parsing start timestamp :\n", exc)
        start = df.index.min()
    else:
        if start < df.index.min():
            start = df.index.min()
        elif start > df.index.max():
            start = df.index.max()

    try:
        end = datetime.strptime(args.end, date_format)
        end = end.replace(tzinfo = timezone('UTC'))
    except Exception as exc :
        print("Main : parsing end timestamp :\n", exc)
        end = df.index.max()
    else:
        if end < df.index.min():
            end = df.index.min()
        elif end > df.index.max():
            end = df.index.max()

    if start > end:
        printf("Try again Joe !")
        exit()

    # Select date range
    df_sel = df[(df.index >= start) & (df.index < end)]

    # Print some information on selection
    print('\ndf_sel information: \n')
    print('\ndf_sel head :\n')
    print(df_sel.head(10))
    print('\ndf_sel tail :\n')
    print(df_sel.tail(10))

    # Adjust figure size
    fig = plt.subplots(figsize = (16, 10))

    # Create a plot
    plt.plot(df_sel.index, df_sel.db1_dbw0, label = 'db1_dbw0')
    plt.plot(df_sel.index, df_sel.ew0, label = 'ew0')
    plt.plot(df_sel.index, df_sel.aw0, label = 'aw0')
    plt.plot(df_sel.index, df_sel.mw0, label = 'mw0')
    plt.title('S7 Communication Example', fontsize = 20)
    plt.xlabel('Timestamp', fontsize = 15)
    plt.ylabel('Value', fontsize = 15)
    plt.xticks(rotation = 45)
    plt.legend()
    plt.grid(True)

    # Save the plot in a file
    plt.savefig('plot1.png')

if __name__ == "__main__":
    main()

